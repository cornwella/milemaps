## Milemaps

Generate mileage maps for common destinations in the Southgate Community School District. Uses Chromedriver and 
Selenium to capture `.png` screenshots of trip maps as required by the Mileage Reimbursement rules.

### Setup & Requirements

Needs [selenium](http://selenium-python.readthedocs.io/) and [googlemaps](https://github.com/googlemaps/google-maps-services-python).

```pip install selenium googlemaps```

The Windows 64-bit Chrome Driver is bundled with the repository; to obtain a different version, visit 
[the official page](https://sites.google.com/a/chromium.org/chromedriver/downloads) and specify its name and
extension on line 78 in the program.

Create a file with a comma-separated list of trip locations:

    Grogan, Fordline, Beacon
    Asher
    Davidson, Beacon
    Beacon, Asher
    
Anderson High School is implied as trip start and end point and should not be specified.

Run the program with:

`milemaps.py (filename)`

Screenshots will be saved in the `/maps` directory as image files named after underscore-joined destination lists.

### Specifying additional destinations

On line 8 (the `locations` dictionary), add a `key` and a `value` pair:

    'St Pius': "St Pius X Catholic School, 14141 Pearl St, Southgate, MI 48195"
    
The `key` is used in the file you provide, while the `value` is what gets passed to Google Maps.