# -*- coding: utf-8

from sys import argv
from selenium import webdriver
import googlemaps

# Google Maps API Key
# Enter your own key here.
api_key = ""

# Location list. The location's "short name" is the key, while the full address used by Maps is the value.
locations = {'Allen': "Allen Elementary School, 16500 McCann Avenue, Southgate, MI 48195",
             'Asher': "Asher School, 14101 Leroy St, Southgate, MI 48195",
             'Anderson': "Southgate Anderson High School, 15475 Leroy Street, Southgate, MI 48195",
             'Beacon': "Gerisch Middle School, 12601 Mccann St, Southgate, MI 48195",
             'Central': "Southgate City Hall, 14700 Reaume Pkwy, Southgate, MI 48195",
             'Davidson': "Davidson Middle School, 15800 Trenton Road, Southgate, MI",
             'Grogan': "Grogan Elementary School, 13300 Burns Street, Southgate, MI",
             'Fordline': "Fordline Elementary School, 14775 Fordline Street, Southgate, MI",
             'Shelters': "Shelters Elementary School, 12600 Fordline Street, Southgate, MI"
             }


def get_distance(destinations):
    total_distance = 0.0

    waypoint_string = ""

    gmaps = googlemaps.Client(api_key)

    # Construct the waypoint string out of all given destinations
    for item in destinations:
        waypoint_string += locations[item] + "|"

    # Trim out the last pipe character
    waypoint_string = waypoint_string[:-1]

    directions_result = gmaps.directions(origin=locations["Anderson"],
                                         destination=locations["Anderson"],
                                         waypoints=waypoint_string,
                                         mode="driving",
                                         units="imperial")

    for legs in directions_result[0].get("legs"):
        leg = legs.get("distance")["text"]
        # If the distance present is in miles ...
        if "mi" in leg:
            leg = leg.replace(" mi", "").replace(",", "")
        # And if it's in feet, for *really* short trips ...
        elif "ft" in leg:
            print "NOTE: Distance in feet! Converting."
            leg = leg.replace(" ft", "")
            leg = (float(leg) * 0.000189394)

        total_distance += float(leg)

    return total_distance


def get_url(destinations):
    address = []

    maps_base_url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&"

    maps_base_url += "origin=" + locations["Anderson"] + "&destination=" + locations["Anderson"] + "&"

    maps_base_url += "waypoints="

    for item in destinations:
        address.append(locations[item])
        maps_base_url += (locations[item] + "|")

    maps_base_url = maps_base_url.replace(" ", "+")

    return maps_base_url


def get_screenshot(destinations, url):
    driver = webdriver.Chrome('./chromedriver.exe')
    driver.set_window_position(0, 0)
    driver.set_window_size(1200, 1000)
    driver.get(url)
    driver.implicitly_wait(3.5)
    screenshot = driver.save_screenshot("./maps/" + "_".join(destinations) + ".png")
    driver.close()


def helptext():
    print '''----------------------------------------------------------------------------------------------
Usage: milemaps.py (file)
The file should contain one comma-separated destination list per line, e.g.:

    Grogan, Fordline, Beacon
    Asher
    Davidson, Beacon
    Beacon, Asher
    
Anderson is the default start and end location for each trip and should not be specified.
----------------------------------------------------------------------------------------------'''


if __name__ == "__main__":
    if len(argv) < 2:
        print("Too few arguments.")
        helptext()
    elif len(argv) == 2:
        filepath = argv[1]
        with open(filepath) as fp:
            for line in fp:
                destination = line.strip()
                destination = destination.split(", ")
                url = get_url(destination)
                get_screenshot(destination, url)
                print(", ".join(destination) + "\t" + str(get_distance(destination)) + " miles")
        exit(0)
    else:
        print("Too many / unknown arguments supplied.")
        helptext()
